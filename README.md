# Utilities to make a consensus from a sequence alignment.

This is a very preliminary version, with very few utilities.


## Installing

Get the source using `git clone git@gitlab.pasteur.fr:bli/libconsensus.git`, `cd` into it and run `python3 -m pip install .`

It might also work directly:

    python3 -m pip install git+ssh://git@gitlab.pasteur.fr/bli/libconsensus.git

