__copyright__ = "Copyright (C) 2020 Blaise Li"
__licence__ = "GNU GPLv3"
__version__ = 0.1
from .libconsensus import (
    ali2cons,
    default_cons_methods,
    fasta2cons,
    )
