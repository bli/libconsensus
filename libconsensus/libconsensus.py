# Copyright (C) 2020 Blaise Li
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
This library provides utilities to generate a consensus
from an alignment of sequences.
"""

import logging
# from datetime import date
from itertools import dropwhile
from functools import partial
from operator import gt, itemgetter, or_
from pathlib import Path
from shutil import copyfileobj
import pandas as pd
from cytoolz import (
    accumulate, compose, dissoc,
    first, frequencies, juxt, reduce)
from mappy import fastx_read


# Numerical values of nucleotides, as powers of 1
A = 1
C = 2
G = 4
T = 8
# Values of ambiguity codes, as sums of powers of 2
R = A + G
Y = C + T
S = G + C
W = A + T
K = G + T
M = A + C
B = C + G + T
D = A + G + T
H = A + C + T
V = A + C + G
N = A + C + G + T
# Converters between numerical values and letters
nucl2val = dict(zip(  # pylint: disable=C0103
    "ACGTRYSWKMBDHVN",
    [A, C, G, T, R, Y, S, W, K, M, B, D, H, V, N])).get
val2nucl = dict(zip(  # pylint: disable=C0103
    [A, C, G, T, R, Y, S, W, K, M, B, D, H, V, N],
    "ACGTRYSWKMBDHVN")).get


def iupac(nucls):
    """
    Compute the IUPAC ambiguity code resulting from the union of *nucls*.
    """
    return val2nucl(reduce(or_, map(nucl2val, nucls)))


def fasta2seqlines(fname):
    """
    Generate lists of letters corresponding to sequences
    in fasta-formatted file *fname*.
    """
    for (_, seq, *_) in fastx_read(str(fname)):
        # Get sequence from next line, convert it to list of letters.
        yield list(seq)


def count(letters, nogap=False):
    """
    Count the letters in *letters* and return a sorted list of
    pairs (letter, count).

    If *nogap* is set to True, gaps are ignored.
    """
    if nogap:
        return sorted(
            dissoc(frequencies(letters), "-").items(),
            key=itemgetter(1), reverse=True)
    return sorted(
        frequencies(letters).items(),
        key=itemgetter(1), reverse=True)


def keyset_add(cumul_counts, counter_item):
    """
    Perform one step of accumulation in a set
    using first elements of tuples.
    """
    return or_(cumul_counts[0], {counter_item[0]})


def counts_add(cumul_counts, counter_item):
    """
    Perform one step of summing
    using second elements of tuples.
    """
    return cumul_counts[1] + counter_item[1]


# This should be updated if `col_consensus` changes.
default_cons_methods = """
A consensus of the aligned sequences was computed using a rather conservative approach:

If one nucleotide accounted for at least 75% of an alignment column, this column was represented by this nucleotide.
Otherwise, the "N" ambiguity code was used.
"""


def col_consensus(column, cum_freq=0.75, do_iupac=False):
    """
    Determine the consensus of the letters present in *column*.

    *cum_freq* is the frequency required for the majority letter
    to be used as consensus.

    When a single letter accounts for the top *cum_freq* letters in the column,
    it is used as consensus.

    If *do_iupac* is set to True, and no "-" is contained in the column,
    then the ambiguity code of the nucleotides comprising the top
    *cum_freq* letters in the column is used. Otherwise, "N" is used.
    """
    # letter_counts = count(column)
    # Number of sequences entering in the consensus
    cons_count = len(column) * cum_freq
    cum_counts = accumulate(
        juxt(keyset_add, counts_add),
        # letter_counts,
        count(column),
        initial=(set(), 0))
    # True if cons_count > the second element
    pred = compose(partial(gt, cons_count), itemgetter(1))
    # Set of letters accounting for this cumulated count
    letters = first(dropwhile(pred, cum_counts))[0]
    try:
        # If there is only one letter, we use it.
        [letter] = letters
        return letter
    except ValueError:
        if "-" not in letters and do_iupac:
            return iupac(letters)
        # Otherwise, consensus will be "N", because
        # we don't know how to handle "-"
        return "N"


def ali2cons(fname):
    """
    Generate the consensus of a sequence alignement present in
    file *fname*.

    *fname* should be the path to a fasta file containing aligned
    sequences.
    """
    ali = pd.DataFrame(fasta2seqlines(fname))
    return "".join(ali.apply(col_consensus))


def fasta2cons(ali_fname, cons_alone, ali_with_cons):
    """
    Generate the consensus of a sequence alignement present in
    file *ali_fname* and write it alone in *cons_alone* as well as
    together with the aligned sequences in *ali_with_cons*.

    *ali_fname* should be the path to a fasta file containing aligned
    sequences.
    """
    # Ensure we have a pathlib.Path object
    ali_fname = Path(ali_fname)
    cons_seq = ali2cons(ali_fname)
    Path(cons_alone).parent.mkdir(parents=True, exist_ok=True)
    with open(cons_alone, "w") as out_fasta:
        out_fasta.write(f">{ali_fname.stem}_consensus\n")
        out_fasta.write(f"{cons_seq.replace('-', '')}\n")
    logging.info("%s written", cons_alone)
    Path(ali_with_cons).parent.mkdir(parents=True, exist_ok=True)
    with open(ali_with_cons, "w") as out_fasta:
        with open(ali_fname) as in_fasta:
            copyfileobj(in_fasta, out_fasta)
        out_fasta.write(f">{ali_fname.stem}_consensus\n")
        out_fasta.write(f"{cons_seq}\n")
    logging.info("%s written", ali_with_cons)
    return str(ali_with_cons)
